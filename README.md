Yet another tool to send SMS through French provider FreeMobile!

[![PyPI](https://img.shields.io/pypi/v/send-sms-freemobile.svg)](https://pypi.org/project/send-sms-freemobile/)
[![PyPI - Status](https://img.shields.io/pypi/status/send-sms-freemobile.svg)](https://pypi.org/project/send-sms-freemobile/)
[![PyPI - License](https://img.shields.io/pypi/l/send-sms-freemobile.svg)](https://opensource.org/licenses/ISC)

## How to use

Just use the following command:

    send-sms
    
If the command is not on your PATH, it will be usually found on `$HOME/.local/bin`.

## Configuration file

Configuration file can be found here:

- Directory: XDG_CONFIG_HOME (default is `$HOME/.config`)
- File: `send-sms-freemobile.conf`

It should look like:

    user: free-user-here
    password: free-password-here

